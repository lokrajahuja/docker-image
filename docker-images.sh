#!/bin/sh

export tags='latest beta 3.1.0.237369.beta 3.0.1.223668 3.0.1.223596.beta 3.0.1.220180.beta 3.0.1.220028 3.0.1.213421 3.0.1.210918.beta 3.0.1.210782.beta 3.0.1.202314 3.0.1.202274.beta 3.0.1.199065.beta 3.0.1.191186 3.0.0.188430.beta 3.0.0.181539 2.0.1.179122.beta 2.0.1.169917.beta'
export images='dx dx-salesforce dx-appirio'
for i in $images
do
	for t in $tags
	do
	    docker pull appirio/$i:$t
	    docker tag appirio/$i:$t registry.gitlab.com/lokrajahuja/docker-image/$i:$t
	    docker push registry.gitlab.com/lokrajahuja/docker-image/$i:$t
	done
done

docker pull appirio/dx:2.0.1.146932
docker tag appirio/dx:2.0.1.146932 registry.gitlab.com/lokrajahuja/docker-image/dx:2.0.1.146932
docker push registry.gitlab.com/lokrajahuja/docker-image/dx:2.0.1.146932